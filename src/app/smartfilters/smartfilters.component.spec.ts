import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartfiltersComponent } from './smartfilters.component';

describe('SmartfiltersComponent', () => {
  let component: SmartfiltersComponent;
  let fixture: ComponentFixture<SmartfiltersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartfiltersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartfiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
